import React, { useEffect, useState } from "react";

function ReactionList(){
    const [reactions, setReactions] = useState([])

    const getReactionData = async() => {
        const response = await fetch("http://localhost:8000/reactions/")
        const data = await response.json()
        setReactions(data.reactions)
    }

    useEffect(()=>{
        getReactionData()
    },[]
    )

    return(

        <div>
            <h1>
                All Reactions
            </h1>
            <table className="table table-success table-hover table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Effect</th>
                    </tr>
                </thead>
                <tbody>
                    {reactions?.map(reaction=> {
                    return(
                    <tr key={reaction.id}>
                        <td> { reaction.name }</td>
                        <td> { reaction.effect }</td>
                    </tr>
                    ) })}
                </tbody>
            </table>
        </div>

    )
}

export default ReactionList;
