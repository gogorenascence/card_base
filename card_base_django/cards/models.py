from django.db import models


class Card_type(models.Model):
    name = models.CharField(max_length=200, unique=True)
    rules = models.TextField(null=True, blank=True)
    deck_type = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Card(models.Model):
    card_name = models.CharField(max_length=200, unique=True)
    card_class = models.CharField(max_length=200)
    hero_name = models.CharField(max_length=200)
    hero_id = models.CharField(max_length=200)
    series_name = models.CharField(max_length=200)
    card_number = models.CharField(max_length=200, null=True, blank=True)
    effect_text = models.CharField(max_length=200, null=True, blank=True)
    flavor_text = models.TextField(null=True, blank=True)
    illustrator = models.CharField(max_length=200, null=True, blank=True)
    picture_url = models.URLField(max_length=3000, null=True, blank=True)
    file_name = models.CharField(max_length=200, null=True, blank=True)
    extra_effects = models.CharField(max_length=200, null=True, blank=True)
    reactions = models.CharField(max_length=200, null=True, blank=True)
    tags = models.CharField(max_length=200, null=True, blank=True)

    card_type = models.ForeignKey(
        Card_type,
        related_name="card",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.card_name
