from django.http import JsonResponse
from .models import Card, Card_type
from .encoders import (
    CardListEncoder,
    CardDetailEncoder,
    Card_typeDetailEncoder
    )
from django.views.decorators.http import require_http_methods
import json


@require_http_methods(["GET", "POST"])
def card_types(request):
    if request.method == "GET":
        card_types = Card_type.objects.all()
        return JsonResponse(
            {"card_types": card_types},
            encoder=Card_typeDetailEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            card_type = Card_type.objects.create(**content)
            return JsonResponse(
               card_type,
               encoder=Card_typeDetailEncoder,
               safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Card type already exists"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def card_type(request, id):
    if request.method == "GET":
        card_type = Card_type.objects.get(id=id)
        return JsonResponse(
            card_type,
            encoder=Card_typeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Card_type.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        Card_type.objects.filter(id=id).update(**content)

        card_type = Card_type.objects.get(id=id)
        return JsonResponse(
            Card_type,
            encoder=Card_typeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def cards(request):
    if request.method == "GET":
        cards = Card.objects.all()
        return JsonResponse(
            {"cards": cards},
            encoder=CardListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            card_type = Card_type.objects.get(name=content["card_type"])
            content["card_type"] = card_type
        except Card_type.DoesNotExist:
            return JsonResponse(
                {"message": "invalid card type"},
                status=404,
            )
        card = Card.objects.create(**content)
        return JsonResponse(
            card,
            encoder=CardDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def card(request, id):
    if request.method == "GET":
        card = Card.objects.get(id=id)
        return JsonResponse(
            card,
            encoder=CardDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Card.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        Card.objects.filter(id=id).update(**content)

        card = Card.objects.get(id=id)
        return JsonResponse(
            card,
            encoder=CardDetailEncoder,
            safe=False,
        )
