from .models import Card_type, Card
from common.json import ModelEncoder


class Card_typeDetailEncoder(ModelEncoder):
    model = Card_type
    properties = ["name", "rules", "deck_type", "id"]


class CardListEncoder(ModelEncoder):
    model = Card
    properties = ["card_name", "hero_name", "card_type", "id"]

    encoders = {
        "card_type": Card_typeDetailEncoder(),
    }


class CardDetailEncoder(ModelEncoder):
    model = Card
    properties = [
        "card_name",
        "card_class",
        "hero_name",
        "hero_id",
        "series_name",
        "card_number",
        "effect_text",
        "flavor_text",
        "illustrator",
        "picture_url",
        "file_name",
        "extra_effects",
        "reactions",
        "tags",
        "card_type",
        ]

    encoders = {
        "card_type": Card_typeDetailEncoder(),
    }
