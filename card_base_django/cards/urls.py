from django.urls import path

from .views import (
    card_types,
    card_type,
    cards,
    card,
)

urlpatterns = [
    path("cardtypes/", card_types, name="card_types"),
    path("cardtype/<int:id>/", card_type, name="card_type"),
    path("cards/", cards, name="cards"),
    path("card/<int:id>/", card, name="card"),
]
