from django.contrib import admin
from .models import Card_type, Card


@admin.register(Card_type)
class Card_typeAdmin(admin.ModelAdmin):
    pass


@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    pass
